import { Component } from '@angular/core';
import { ApiListService } from './providers/api-list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'infiR';
  limit = 1;
  Listdata = [];
  ListView = true;
  GridView = false;
  Showtext = false;
  query: string = '';

  constructor(public backEndApi: ApiListService) { }

  ngOnInit() {
    this.getApiData(this.limit);
  }

  getApiData(pageData) {
    this.backEndApi.FetchPageDetails(pageData).subscribe(
      prods => {
        console.log(prods);
        this.Listdata = prods.data;
        if (this.Listdata.length === 0) {
          this.Showtext = true;
        }
      },
      err => {
        console.log("Error", err);
      }
    );
  }

  SetPage(page) {
    this.limit = page;
    this.getApiData(this.limit);
    this.Showtext = false;
  }

  Show(View) {
    if (View === 'Grid') {
      this.GridView = true;
      this.ListView = false;
    } else {
      this.ListView = true;
      this.GridView = false;
    }
  }

  Previous() {
    this.limit--;
    this.getApiData(this.limit);
    this.Showtext = false;
  }

  Next() {
    this.limit++;
    this.getApiData(this.limit);
  }
}
