import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ApiListService {
  readonly base_url = 'https://reqres.in/api';
  m_Headers: any;
  m_Options: any;

  constructor(public http: Http) {
    console.log(this.base_url);
    this.m_Headers = new Headers({ 'Content-Type': 'application/json' });//x-www-form-urlencoded //application/json'
    this.m_Options = new RequestOptions({ headers: this.m_Headers }); //method : RequestMethod.Post 
  }

  FetchPageDetails(limit): Observable<any> {
    let count = 'page=' + limit;
    return this.http.get(this.base_url + '/users?' + count).map(res => <any>res.json());
  }

}

